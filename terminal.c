#include <system.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

/*Macro variables----------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_fputchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

#ifndef CONFIG_CONSOLE_BUFSZ
#define CONFIG_CONSOLE_BUFSZ    100
#endif



static stdio_t* terminal_stdio = NULL;

PUTCHAR_PROTOTYPE
{
    terminal_putchar(ch);
    return ch;
}

void terminal_init(stdio_t* stdio)
{
    ASSERT(stdio);
    ASSERT(stdio->put_char);
    terminal_stdio = stdio;
}

int terminal_putchar(int ch)
{
    if (terminal_stdio && terminal_stdio->put_char)
        return terminal_stdio->put_char(ch);
    return -1;
}


int terminal_getchar(void)
{
    if (terminal_stdio && terminal_stdio->get_char)
        return terminal_stdio->get_char();
    return -1;
}

int terminal_write(const char* wr_data, ze_size_t wr_len)
{
    int i = 0;
    for (i = 0; i < wr_len; i++)
    {
        if (terminal_putchar(wr_data[i]) < 0)
            break;
    }
    return i;
}

int print(const char* fmt, ...)
{
    static char buffer[CONFIG_CONSOLE_BUFSZ];
    int ret = 0;
    va_list args;
    va_start(args, fmt);
    ret = vsnprintf((char*)buffer, sizeof(buffer) - 1, fmt, args);
    va_end(args);
    return terminal_write(buffer, ret);
}

