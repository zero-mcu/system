/**
 * @file system.h
 * @brief This is the difinition of system.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/05/30    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-05-30
 * @license Dual BSD/GPL
 */
#ifndef __SYSTEM_H__
#define __SYSTEM_H__
#include <system/syscfg.h>
#include <system/types.h>
#include <system/assert.h>
#include <system/co_thread.h>
#include <system/errno.h>
#include <system/time.h>
#include <system/terminal.h>

#if CONFIG_USING_RTOS == 1
#include <cmsis_os.h>
#endif

void system_schedule(void);
#endif