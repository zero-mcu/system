/**
 * @file terminal.h
 * @brief This is the difinition of terminal.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/05/31    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-05-31
 * @license Dual BSD/GPL
 */
#ifndef __SYSTEM_TERMINAL_H__
#define __SYSTEM_TERMINAL_H__
#include <system/types.h>
#ifdef __cplusplus
extern "C"{
#endif

typedef struct {
    int (*get_char)(void);
    int (*put_char)(int ch);
} stdio_t;

void terminal_init(stdio_t* stdio);

int terminal_putchar(int ch);

int terminal_getchar(void);

int terminal_write(const char* wr_data, ze_size_t wr_len);

/**
 * @brief print format string to console.
 * @param fmt format string.
 * @return number of char has been print. if retval < 0,
 * execute error.
 */
int print(const char* fmt, ...);

#ifdef __cplusplus
} // extern "C"
#endif
#endif // __SYSTEM_TERMINAL_H__