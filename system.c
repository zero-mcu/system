#include <system.h>

void system_schedule(void)
{
#if CONFIG_USING_RTOS == 0
    while(1)
    {
        co_thread_schedule();
    }
#else
    osKernelStart();
#endif
}